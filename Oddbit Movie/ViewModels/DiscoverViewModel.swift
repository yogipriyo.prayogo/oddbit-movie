//
//  DiscoverViewModel.swift
//  Oddbit Movie
//
//  Created by Yogi Priyo Prayogo on 11/01/22.
//

import Foundation
import Alamofire

protocol DiscoverViewModelDelegate: AnyObject {
    func getDiscoverMoviesFinished(data: [DiscoverMovie]?, errorNotes: String?)
}

class DiscoverViewModel: NSObject {
    weak var delegate: DiscoverViewModelDelegate?
    
    func getDiscoverMovies(sortBy: String, fromDate: String = "", toDate: String = "") {
        AF.request(NetworkRouter.getDiscoverMovies(sortBy: sortBy, fromDate: fromDate, toDate: toDate))
            .responseDecodable { [weak self] (response: AFDataResponse<BaseResponse<DiscoverMovie>>) in
                switch response.result {
                case .success(let origin):
                    self?.delegate?.getDiscoverMoviesFinished(data: origin.results, errorNotes: nil)
                case .failure(let error):
                    self?.delegate?.getDiscoverMoviesFinished(data: nil, errorNotes: error.localizedDescription)
                }
        }
    }
}

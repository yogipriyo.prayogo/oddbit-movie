//
//  GenreViewModel.swift
//  Oddbit Movie
//
//  Created by Yogi Priyo Prayogo on 11/01/22.
//

import Foundation
import Alamofire

protocol GenreViewModelDelegate: AnyObject {
    func getGenresFinished(data: [GenreDetails]?, errorNotes: String?)
}

class GenreViewModel: NSObject {
    weak var delegate: GenreViewModelDelegate?
    
    func getGenres() {
        AF.request(NetworkRouter.getGenres)
            .responseDecodable { [weak self] (response: AFDataResponse<Genre>) in
                switch response.result {
                case .success(let origin):
                    self?.delegate?.getGenresFinished(data: origin.genres, errorNotes: nil)
                case .failure(let error):
                    self?.delegate?.getGenresFinished(data: nil, errorNotes: error.localizedDescription)
                }
        }
    }
}

//
//  DetailsViewModel.swift
//  Oddbit Movie
//
//  Created by Yogi Priyo Prayogo on 20/01/22.
//

import Foundation
import Alamofire

protocol DetailsViewModelDelegate: AnyObject {
    func getDetailsFinished(data: MovieDetails?, errorNotes: String?)
}

class DetailsViewModel: NSObject {
    weak var delegate: DetailsViewModelDelegate?
    
    func getDetails(movieId: Int) {
        AF.request(NetworkRouter.getDetails(movieId: movieId))
            .responseDecodable { [weak self] (response: AFDataResponse<MovieDetails>) in
                switch response.result {
                case .success(let origin):
                    self?.delegate?.getDetailsFinished(data: origin, errorNotes: nil)
                case .failure(let error):
                    self?.delegate?.getDetailsFinished(data: nil, errorNotes: error.localizedDescription)
                }
        }
    }
}

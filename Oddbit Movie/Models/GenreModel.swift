//
//  GenreModel.swift
//  Oddbit Movie
//
//  Created by Yogi Priyo Prayogo on 11/01/22.
//

import Foundation

struct Genre: Codable {
    let genres: [GenreDetails]
}

struct GenreDetails: Codable {
    let id: Int?
    let name: String?
}

//
//  DetailsModel.swift
//  Oddbit Movie
//
//  Created by Yogi Priyo Prayogo on 20/01/22.
//

import Foundation

struct MovieDetails: Codable {
    let adult: Bool?
    let backdropPath: String?
    let genres: [GenreDetails]?
    let homepage: String?
    let id: Int?
    let title: String?
    let overview: String?
    let popularity: Double?
    let posterPath: String?
    let productionCompanies: [ProductionCompany]?
    let releaseDate: String?
    let revenue: Double?
    let runtime: Int?
    let spokenLanguages: [SpokenLanguage]?
    let tagline: String?
    let voteAverage: Double?
    let voteCount: Int?
    
    enum CodingKeys: String, CodingKey {
        case adult
        case backdropPath = "backdrop_path"
        case genres
        case homepage
        case id
        case title
        case overview
        case popularity
        case posterPath = "poster_path"
        case productionCompanies = "production_companies"
        case releaseDate = "release_date"
        case revenue
        case runtime
        case spokenLanguages = "spoken_languages"
        case tagline
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
    
    struct SpokenLanguage: Codable {
        let name: String?
        let iso6391: String?
        let englishName: String?
        
        enum CodingKeys: String, CodingKey {
            case name
            case iso6391 = "iso_639_1"
            case englishName = "english_name"
        }
    }
    
    struct ProductionCompany: Codable {
        let id: Int?
        let logoPath: String?
        let name: String?
        let originCountry: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case logoPath = "logo_path"
            case name
            case originCountry = "origin_country"
        }
    }
}

//
//  ViewControllerExtensions.swift
//  Oddbit Movie
//
//  Created by Yogi Priyo Prayogo on 11/01/22.
//

import UIKit

extension UIViewController {
    func displayLoading() {
        let loadingVC: LoadingViewController = LoadingViewController()
        loadingVC.modalPresentationStyle = .overCurrentContext
        loadingVC.modalTransitionStyle = .crossDissolve
        
        DispatchQueue.main.async { [weak self] in
            self?.present(loadingVC, animated: true, completion: nil)
        }
    }
    
    func hideLoading() {
        self.dismiss(animated: true, completion: nil)
    }
}

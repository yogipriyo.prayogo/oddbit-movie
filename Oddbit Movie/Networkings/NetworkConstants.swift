//
//  NetworkConstants.swift
//  Oddbit Movie
//
//  Created by Yogi Priyo Prayogo on 11/01/22.
//

import Foundation
import Alamofire

struct NetworkConstants {
    struct ProductionServer {
        static let baseURL = "https://api.themoviedb.org/3"
        static let apiKey = "1b869b3ccf57d089047ded4b1de007b8"
        static let imageBaseURL = "https://image.tmdb.org/t/p/w500"
    }
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
    case string = "String"
    
}

enum ContentType: String {
    case json = "Application/json"
    case formEncode = "application/x-www-form-urlencoded"
}

enum RequestParams {
    case body(_:Parameters)
    case url(_:Parameters)
}

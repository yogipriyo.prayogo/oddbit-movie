//
//  NetworkRouter.swift
//  Oddbit Movie
//
//  Created by Yogi Priyo Prayogo on 11/01/22.
//

import Foundation
import Alamofire

enum NetworkRouter: NetworkConfiguration {
    
    case getDiscoverMovies(sortBy: String, fromDate: String, toDate: String)
    case getGenres
    case getDetails(movieId: Int)
    
    // MARK: - HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .getDiscoverMovies, .getGenres, .getDetails:
            return .get
        }
    }
    // MARK: - Parameters
     var parameters: RequestParams {
        switch self {
        case .getDiscoverMovies(let sortBy, let fromDate, let toDate):
            return .url(["api_key":NetworkConstants.ProductionServer.apiKey, "sort_by": sortBy, "release_date.lte": toDate, "release_date.gte": fromDate])
        case .getGenres, .getDetails:
            return .url(["api_key":NetworkConstants.ProductionServer.apiKey])
        }
    }
    
    // MARK: - Path
    var path: String {
        switch self {
        case .getDiscoverMovies:
            return "/discover/movie"
        case .getGenres:
            return "/genre/movie/list"
        case .getDetails(let movieId):
            return "/movie/\(movieId)"
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try NetworkConstants.ProductionServer.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        urlRequest.cachePolicy = .useProtocolCachePolicy
        
        
        // Parameters
        switch parameters {
            
        case .body( _):
            break
            
        case .url(let params):
                let queryParams = params.map { pair  in
                    return URLQueryItem(name: pair.key, value: "\(pair.value)")
                }
                var components = URLComponents(string:url.appendingPathComponent(path).absoluteString)
                components?.queryItems = queryParams
                urlRequest.url = components?.url
        }
            return urlRequest
    }
}

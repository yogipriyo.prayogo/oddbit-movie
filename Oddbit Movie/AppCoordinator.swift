//
//  AppCoordinator.swift
//  Oddbit Movie
//
//  Created by Yogi Priyo Prayogo on 10/01/22.
//

import UIKit

class AppCoordinator {
    private let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        let homeVC: HomeViewController = HomeViewController()
        let navigationController: UINavigationController = UINavigationController(rootViewController: homeVC)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}

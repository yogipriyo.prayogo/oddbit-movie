//
//  MovieTableViewCell.swift
//  Oddbit Movie
//
//  Created by Yogi Priyo Prayogo on 11/01/22.
//

import UIKit
import SDWebImage

protocol MovieTableViewCellDelegate: class {
    func addItemToFavorite(_ data: DiscoverMovie)
}

final class MovieTableViewCell: UITableViewCell {
    
    enum OriginType {
        case discover
        case favorite
    }
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var popularityLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addToFavoriteButton: UIButton!
    
    var delegate: MovieTableViewCellDelegate?
    private var data: DiscoverMovie?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    private func setupViews() {
        containerView.layer.cornerRadius = 5
        selectionStyle = .none
        addToFavoriteButton.layer.cornerRadius = 8
    }
    
    internal func setupContent(_ data: DiscoverMovie, originType: OriginType) {
        self.data = data
        titleLabel.text = data.title
        if let posterPath = data.posterPath {
            posterImage.sd_setImage(with: URL(string: NetworkConstants.ProductionServer.imageBaseURL+posterPath ), placeholderImage: UIImage(named: "no-image-found"), completed: nil)
        }
        descriptionLabel.text = data.overview
        containerViewHeight.constant = ( originType == .discover ? 165 : 135) + descriptionLabel.requiredHeight
        addToFavoriteButton.isHidden = originType == .favorite
        popularityLabel.text = "Popularity : \(data.popularity ?? 0)"
        genresLabel.text = "Genre : \(data.genreTitles ?? "")"
    }
    
    @IBAction func addToFavoriteTapped(_ sender: UIButton) {
        if let data = data {
            delegate?.addItemToFavorite(data)
        }
    }
    
}

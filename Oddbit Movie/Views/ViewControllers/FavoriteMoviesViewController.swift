//
//  FavoriteMoviesViewController.swift
//  Oddbit Movie
//
//  Created by Yogi Priyo Prayogo on 24/01/22.
//

import UIKit
import CoreData
import Toaster

final class FavoriteMoviesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var favoriteMovieTableView: UITableView!
    
    private let movieCellIdentifier: String = "movieCell"
    private var favoriteMovies: [DiscoverMovie] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getFavoriteMovies()
        setupNavbar()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupTableView()
    }

    // MARK: - Private funcs
    private func setupNavbar() {
        title = "Favorite Movies"
    }
    
    private func setupTableView() {
        favoriteMovieTableView.delegate = self
        favoriteMovieTableView.dataSource = self
        favoriteMovieTableView.register(UINib(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: movieCellIdentifier)
        favoriteMovieTableView.tableFooterView = UIView()
    }
    
    private func getFavoriteMovies() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MovieEntity")
        
        request.returnsObjectsAsFaults = false
        do {
            let results = try context.fetch(request)
            if !results.isEmpty {
                for result in results as! [NSManagedObject] {
                    guard let adult = result.value(forKey: "adult") as? Bool else { return }
                    guard let backdropPath = result.value(forKey: "backdropPath") as? String else { return }
                    guard let genreTitles = result.value(forKey: "genreTitles") as? String else { return }
                    guard let id = result.value(forKey: "id") as? Int else { return }
                    guard let overview = result.value(forKey: "overview") as? String else { return }
                    guard let popularity = result.value(forKey: "popularity") as? Double else { return }
                    guard let posterPath = result.value(forKey: "posterPath") as? String else { return }
                    guard let releaseDate = result.value(forKey: "releaseDate") as? String else { return }
                    guard let title = result.value(forKey: "title") as? String else { return }
                        guard let voteAverage = result.value(forKey: "voteAverage") as? Double else { return }
                    guard let voteCount = result.value(forKey: "voteCount") as? Int else { return }
                    
                    let favMovie: DiscoverMovie = DiscoverMovie(adult: adult, backdropPath: backdropPath, genreIds: nil, genreTitles: genreTitles, id: id, originalLanguage: nil, originalTitle: nil, overview: overview, popularity: popularity, posterPath: posterPath, releaseDate: releaseDate, title: title, video: nil, voteAverage: voteAverage, voteCount: voteCount)
                        favoriteMovies.append(favMovie)
                }
            }
        } catch {
            Toast(text: "Failed to get movie details : \(error)", delay: Delay.short, duration: Delay.long).show()
        }
    }
    
    // MARK: Table view protocol
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoriteMovies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: movieCellIdentifier, for: indexPath as IndexPath) as! MovieTableViewCell
        cell.setupContent(favoriteMovies[indexPath.row], originType: MovieTableViewCell.OriginType.favorite)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//
//  MovieDetailsViewController.swift
//  Oddbit Movie
//
//  Created by Yogi Priyo Prayogo on 20/01/22.
//

import UIKit
import SDWebImage
import TTTAttributedLabel
import CoreData
import Toaster

final class MovieDetailsViewController: UIViewController, DetailsViewModelDelegate {
    
    // MARK: - Properties & outlets
    @IBOutlet weak var backdropImageView: UIImageView!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var overlayContainerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tagline: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var popularityLabel: UILabel!
    @IBOutlet weak var runtimeLabel: UILabel!
    @IBOutlet weak var productionCompaniesLabel: UILabel!
    @IBOutlet weak var voteAverageLabel: UILabel!
    @IBOutlet weak var revenueLabel: UILabel!
    @IBOutlet weak var voteCountLabel: UILabel!
    @IBOutlet weak var spokenLanguagesLabel: UILabel!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var movieUrlLabel: TTTAttributedLabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var textBasedContentHeight: NSLayoutConstraint!
    
    private let movieDetailsViewModel: DetailsViewModel = DetailsViewModel()
    private var movieId: Int
    private var movieDetails: MovieDetails?
    private var genreTitles: String = ""
    
    
    init(movieId: Int) {
        self.movieId = movieId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        movieDetailsViewModel.getDetails(movieId: self.movieId)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupViews()
        setupTapGesture()
        movieDetailsViewModel.delegate = self
    }
    
    @IBAction func addFavoriteButtonTapped(_ sender: UIButton) {
        saveToFavorites()
    }
    
    // MARK: - Private funcs
    private func setupTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(MovieDetailsViewController.openLink))
        movieUrlLabel.isUserInteractionEnabled = true
        movieUrlLabel.addGestureRecognizer(tap)
    }
    
    @objc private func openLink(sender:UITapGestureRecognizer) {
        if let movieDetails = self.movieDetails, let url = URL(string: movieDetails.homepage ?? "") {
            UIApplication.shared.open(url)
        }
    }
    
    private func saveToFavorites() {
        guard let movieDetails = movieDetails else { return }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let newFavorite = NSEntityDescription.insertNewObject(forEntityName: "MovieEntity", into: context)
        newFavorite.setValue(movieDetails.adult, forKey: "adult")
        newFavorite.setValue(movieDetails.backdropPath, forKey: "backdropPath")
        newFavorite.setValue(genreTitles, forKey: "genreTitles")
        newFavorite.setValue(movieDetails.id, forKey: "id")
        newFavorite.setValue(movieDetails.overview, forKey: "overview")
        newFavorite.setValue(movieDetails.popularity, forKey: "popularity")
        newFavorite.setValue(movieDetails.posterPath, forKey: "posterPath")
        newFavorite.setValue(movieDetails.releaseDate, forKey: "releaseDate")
        newFavorite.setValue(movieDetails.title, forKey: "title")
        newFavorite.setValue(movieDetails.voteAverage, forKey: "voteAverage")
        newFavorite.setValue(movieDetails.voteCount, forKey: "voteCount")
        
        do {
            try context.save()
            Toast(text: "Movie has been added to favorites", delay: Delay.short, duration: Delay.long).show()
        } catch {
            Toast(text: "Failed to add movie to favorites : \(error)", delay: Delay.short, duration: Delay.long).show()
        }
    }
    
    private func setupViews() {
        title = ""
        overlayContainerView.layer.cornerRadius = 8
    }
    
    private func populateContent(data: MovieDetails) {
        title = data.title
        titleLabel.text = data.title
        
        if let posterPath = data.posterPath {
            posterImageView.sd_setImage(with: URL(string: NetworkConstants.ProductionServer.imageBaseURL+posterPath), placeholderImage: UIImage(named: "no-image-found"), options: .delayPlaceholder, completed: nil)
        }
        
        if let backdropPath = data.backdropPath {
            backdropImageView.sd_setImage(with: URL(string: NetworkConstants.ProductionServer.imageBaseURL+backdropPath), placeholderImage: UIImage(named: "no-image-found"), options: .delayPlaceholder, completed: nil)
        }
        
        movieUrlLabel.text = data.homepage
        if let homepage = data.homepage,  let movieUrl = URL(string: homepage) {
            let nsString = data.homepage! as NSString
            let range = nsString.range(of: homepage)
            movieUrlLabel.addLink(to: movieUrl, with: range)
        }
        
        tagline.text = data.tagline
        releaseDateLabel.text! += data.releaseDate ?? ""
        popularityLabel.text! += String(data.popularity ?? 0)
        descriptionLabel.text = data.overview
        voteAverageLabel.text! += String(data.voteAverage ?? 0)
        voteCountLabel.text! += String(data.voteCount ?? 0)
        revenueLabel.text! += data.revenue?.convertToCurrency() ?? "$0.00"
        
        let runtimeTuple = minutesToHoursAndMinutes(data.runtime ?? 0)
        runtimeLabel.text! += "\(runtimeTuple.hours) hour \(runtimeTuple.leftMinutes) minute"
        
        if let genres = data.genres {
            for (index, genre) in genres.enumerated() {
                genreLabel.text! += index == 0 ? genre.name ?? "" : ", \(genre.name ?? "")"
                genreTitles += index == 0 ? genre.name ?? "" : ", \(genre.name ?? "")"
            }
        }
        
        if let companies = data.productionCompanies {
            for (index, company) in companies.enumerated() {
                productionCompaniesLabel.text! += index == 0 ? company.name ?? "" : ", \(company.name ?? "")"
            }
        }
        
        if let spokenLanguages = data.spokenLanguages {
            for (index, language) in spokenLanguages.enumerated() {
                spokenLanguagesLabel.text! += index == 0 ? language.name ?? "" : ", \(language.name ?? "")"
            }
        }
        
        textBasedContentHeight.constant = 185 + descriptionLabel.requiredHeight + genreLabel.requiredHeight + productionCompaniesLabel.requiredHeight + spokenLanguagesLabel.requiredHeight
    }
    
    func minutesToHoursAndMinutes (_ minutes : Int) -> (hours : Int , leftMinutes : Int) {
        return (minutes / 60, (minutes % 60))
    }
    
    // MARK: - DetailsViewModelDelegate
    func getDetailsFinished(data: MovieDetails?, errorNotes: String?) {
        if let data = data {
            movieDetails = data
            populateContent(data: data)
        } else {
            Toast(text: "Failed to get movie details : \(errorNotes)", delay: Delay.short, duration: Delay.long).show()
        }
    }
}

//
//  HomeViewController.swift
//  Oddbit Movie
//
//  Created by Yogi Priyo Prayogo on 10/01/22.
//

import UIKit
import Fastis
import CoreData
import Toaster

final class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DiscoverViewModelDelegate, GenreViewModelDelegate, MovieTableViewCellDelegate {
    
    enum SortByOption: String {
        case popularityAsc = "popularity.asc"
        case popularityDesc = "popularity.desc"
        case releaseDateAsc = "release_date.asc"
        case releaseDateDesc = "release_date.desc"
        case voteCountAsc = "vote_count.asc"
        case voteCountDesc = "vote_count.desc"
    }
    
    @IBOutlet weak var topContainerView: UIView!
    @IBOutlet weak var movieTableView: UITableView!
    @IBOutlet weak var sortButton: UIButton!
    
    private let movieCellIdentifier: String = "movieCell"
    private let discoverViewModel: DiscoverViewModel = DiscoverViewModel()
    private var discoverMovieList: [DiscoverMovie] = []
    private let genreViewModel: GenreViewModel = GenreViewModel()
    private var genreList: [GenreDetails] = []
    private var activeSortOption: SortByOption = .popularityDesc
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavbar()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        displayLoading()
        
        setupTableView()
        setupDelegates()
        
        genreViewModel.getGenres()
    }

    // MARK: - Private funcs
    private func displayError(_ errorDescription: String) {
        Toast(text: errorDescription, delay: Delay.short, duration: Delay.long).show()
    }
    
    @objc private func favoriteTapped() {
        let favMoviesVC: FavoriteMoviesViewController = FavoriteMoviesViewController()
        navigationController?.pushViewController(favMoviesVC, animated: true)
    }
    
    private func setupNavbar() {
        title = "Discover Movies"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Favorite", style: .plain, target: self, action: #selector(favoriteTapped))
    }
    
    private func setupDelegates() {
        discoverViewModel.delegate = self
        genreViewModel.delegate = self
    }
    
    private func setupTableView() {
        movieTableView.delegate = self
        movieTableView.dataSource = self
        movieTableView.register(UINib(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: movieCellIdentifier)
        movieTableView.tableFooterView = UIView()
    }
    
    private func populateGenreForMovies(_ movies: [DiscoverMovie]) {
        discoverMovieList = movies
        var genreTitles: String = ""
        for (index, movie) in movies.enumerated() {
            guard let genreIds = movie.genreIds else { return }
            for genreId in genreIds {
                for genreItem in genreList where genreItem.id == genreId {
                    if let genreName = genreItem.name {
                        genreTitles += genreName+" "
                    }
                }
            }
            discoverMovieList[index].genreTitles = genreTitles
            genreTitles = ""
        }
        movieTableView.reloadData()
    }
    
    private func displaySortMenu() {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        let popularityAscAction = UIAlertAction(title: "Popularity (asc)", style: .default, handler:{ [weak self] (UIAlertAction)in
            self?.displayLoading()
            self?.sortButton.setTitle("Popularity (asc)", for: .normal)
            self?.activeSortOption = .popularityAsc
            self?.discoverViewModel.getDiscoverMovies(sortBy: SortByOption.popularityAsc.rawValue)
        })
        let popularityDescAction = UIAlertAction(title: "Popularity (desc)", style: .default, handler:{ [weak self] (UIAlertAction)in
            self?.displayLoading()
            self?.sortButton.setTitle("Popularity (desc)", for: .normal)
            self?.activeSortOption = .popularityDesc
            self?.discoverViewModel.getDiscoverMovies(sortBy: SortByOption.popularityDesc.rawValue)
        })
        let releaseAscAction = UIAlertAction(title: "Release Date (asc)", style: .default, handler:{ [weak self] (UIAlertAction)in
            self?.displayLoading()
            self?.sortButton.setTitle("Release Date (asc)", for: .normal)
            self?.activeSortOption = .releaseDateAsc
            self?.discoverViewModel.getDiscoverMovies(sortBy: SortByOption.releaseDateAsc.rawValue)
        })
        let releaseDescAction = UIAlertAction(title: "Release Date (desc)", style: .default, handler:{ [weak self] (UIAlertAction)in
            self?.displayLoading()
            self?.sortButton.setTitle("Release Date (desc)", for: .normal)
            self?.activeSortOption = .releaseDateDesc
            self?.discoverViewModel.getDiscoverMovies(sortBy: SortByOption.releaseDateDesc.rawValue)
        })
        let voteAscAction = UIAlertAction(title: "Vote Count (asc)", style: .default, handler:{ [weak self] (UIAlertAction)in
            self?.displayLoading()
            self?.sortButton.setTitle("Vote Count (asc)", for: .normal)
            self?.activeSortOption = .voteCountAsc
            self?.discoverViewModel.getDiscoverMovies(sortBy: SortByOption.voteCountAsc.rawValue)
        })
        let voteDescAction = UIAlertAction(title: "Vote Count (desc)", style: .default, handler:{ [weak self] (UIAlertAction)in
            self?.displayLoading()
            self?.sortButton.setTitle("Vote Count (desc)", for: .normal)
            self?.activeSortOption = .voteCountDesc
            self?.discoverViewModel.getDiscoverMovies(sortBy: SortByOption.voteCountDesc.rawValue)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        optionMenu.addAction(popularityAscAction)
        optionMenu.addAction(popularityDescAction)
        optionMenu.addAction(releaseAscAction)
        optionMenu.addAction(releaseDescAction)
        optionMenu.addAction(voteAscAction)
        optionMenu.addAction(voteDescAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func chooseDate() {
        let fastisController = FastisController(mode: .range)
        fastisController.title = "Choose range"
        fastisController.maximumDate = Date()
        fastisController.shortcuts = [.today, .lastWeek]
        fastisController.doneHandler = { [weak self] resultRange in
            let formatter = DateFormatter()
            formatter.dateFormat = "YYYY-MM-dd"
            if let resultRange = resultRange, let self = self {
                let fromDate = formatter.string(from: resultRange.fromDate)
                let toDate = formatter.string(from: resultRange.toDate)
                self.discoverViewModel.getDiscoverMovies(
                    sortBy: self.activeSortOption.rawValue, fromDate: fromDate, toDate: toDate
                )
            }
        }
        fastisController.present(above: self)
    }
    
    private func saveToFavorites(_ movieDetails: DiscoverMovie) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let newFavorite = NSEntityDescription.insertNewObject(forEntityName: "MovieEntity", into: context)
        newFavorite.setValue(movieDetails.adult, forKey: "adult")
        newFavorite.setValue(movieDetails.backdropPath, forKey: "backdropPath")
        newFavorite.setValue(movieDetails.genreTitles, forKey: "genreTitles")
        newFavorite.setValue(movieDetails.id, forKey: "id")
        newFavorite.setValue(movieDetails.overview, forKey: "overview")
        newFavorite.setValue(movieDetails.popularity, forKey: "popularity")
        newFavorite.setValue(movieDetails.posterPath, forKey: "posterPath")
        newFavorite.setValue(movieDetails.releaseDate, forKey: "releaseDate")
        newFavorite.setValue(movieDetails.title, forKey: "title")
        newFavorite.setValue(movieDetails.voteAverage, forKey: "voteAverage")
        newFavorite.setValue(movieDetails.voteCount, forKey: "voteCount")
        
        do {
            try context.save()
            Toast(text: "Movie has been added to favorites", delay: Delay.short, duration: Delay.long).show()
        } catch {
            Toast(text: "Failed to add movie to favorites : \(error)", delay: Delay.short, duration: Delay.long).show()
        }
    }
    
    @IBAction func dateFilterTapped(_ sender: UIButton) {
        chooseDate()
    }
    
    @IBAction func sortedButtonTapped(_ sender: UIButton) {
        displaySortMenu()
    }
    
    // MARK: Table view protocol
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discoverMovieList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: movieCellIdentifier, for: indexPath as IndexPath) as! MovieTableViewCell
        cell.setupContent(discoverMovieList[indexPath.row], originType: MovieTableViewCell.OriginType.discover)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let movieId = discoverMovieList[indexPath.row].id else { return }
        let detailsVC: MovieDetailsViewController = MovieDetailsViewController(movieId: movieId)
        navigationController?.pushViewController(detailsVC, animated: true)
    }
    
    // MARK: - DiscoverViewModelDelegate
    func getDiscoverMoviesFinished(data: [DiscoverMovie]?, errorNotes: String?) {
        hideLoading()
        
        if let data = data { populateGenreForMovies(data) }
        
        if let errorNotes = errorNotes { displayError(errorNotes) }
    }
    
    // MARK: - GenreViewModelDelegate
    func getGenresFinished(data: [GenreDetails]?, errorNotes: String?) {
        if let data = data { genreList = data }
        
        if let errorNotes = errorNotes { displayError(errorNotes) }
        
        discoverViewModel.getDiscoverMovies(sortBy: SortByOption.popularityDesc.rawValue)
    }
    
    // MARK: - MovieTableViewCellDelegate
    func addItemToFavorite(_ data: DiscoverMovie) {
        saveToFavorites(data)
    }
}
